#include "stdafx.h"
#include "DES.h"
const int DES::IP[64] = { 58,	50,	42,	34,	26,	18,	10,	2,
						60,	52,	44,	36,	28,	20,	12,	4,
						62,	54,	46,	38,	30,	22,	14,	6,
						64,	56,	48,	40,	32,	24,	16,	8,
						57,	49,	41,	33,	25,	17,	9,	1,
						59,	51,	43,	35,	27,	19,	11,	3,
						61,	53,	45,	37,	29,	21,	13,	5,
						63,	55,	47,	39,	31,	23,	15,	7 };

const int DES::FP[64] = { 40,	8,	48,	16,	56,	24,	64,	32,
						39,	7,	47,	15,	55,	23,	63,	31,
						38,	6,	46,	14,	54,	22,	62,	30,
						37,	5,	45,	13,	53,	21,	61,	29,
						36,	4,	44,	12,	52,	20,	60,	28,
						35,	3,	43,	11,	51,	19,	59,	27,
						34,	2,	42,	10,	50,	18,	58,	26,
						33,	1,	41,	9,	49,	17,	57,	25 };

const int DES::E[48] = { 32,	1,	2,	3,	4,	5,
						4,	5,	6,	7,	8,	9,
						8,	9,	10,	11,	12,	13,
						12,	13,	14,	15,	16,	17,
						16, 17,	18,	19,	20,	21,
						20,	21,	22,	23,	24,	25,
						24,	25,	26,	27,	28,	29,
						28,	29,	30,	31,	32,	1 };

const int DES::P[32] = { 16,	7,	20,	21,	29,	12,	28,	17,
						1,	15,	23,	26,	5,	18,	31,	10,
						2,	8,	24,	14,	32,	27,	3,	9,
						19,	13,	30,	6,	22,	11,	4,	25 };

const int DES::PC1L[28] = { 57,	49,	41,	33,	25,	17,	9,
							1,	58,	50,	42,	34,	26,	18,
							10,	2,	59,	51,	43,	35,	27,
							19,	11,	3,	60,	52,	44,	36 };

const int DES::PC1R[28] = { 63,	55,	47,	39,	31,	23,	15,
							7,	62,	54,	46,	38,	30,	22,
							14,	6,	61,	53,	45,	37,	29,
							21,	13,	5,	28,	20,	12,	4 };

const int DES::PC2[48] = { 14,	17,	11,	24,	1,	5,
							3,	28,	15,	6,	21,	10,
							23,	19,	12,	4,	26,	8,
							16,	7,	27,	20,	13,	2,
							41,	52,	31,	37,	47,	55,
							30,	40,	51,	45,	33,	48,
							44,	49,	39,	56,	34,	53,
							46,	42,	50,	36,	29,	32 };

const int DES::S1[4][16] = { { 14,	4,	13,	1,	2,	15,	11,	8,	3,	10,	6,	12,	5,	9,	0,	7 },
							{ 0,	15,	7,	4,	14,	2,	13,	1,	10,	6,	12,	11,	9,	5,	3,	8 },
							{ 4,	1,	14,	8,	13,	6,	2,	11,	15,	12,	9,	7,	3,	10,	5,	0 },
							{ 15,	12,	8,	2,	4,	9,	1,	7,	5,	11,	3,	14,	10,	0,	6,	13 } };

const int DES::S2[4][16] = { { 15,	1,	8,	14,	6,	11,	3,	4,	9,	7,	2,	13,	12,	0,	5,	10 },
							{ 3,	13,	4,	7,	15,	2,	8,	14,	12,	0,	1,	10,	6,	9,	11,	5 },
							{ 0,	14,	7,	11,	10,	4,	13,	1,	5,	8,	12,	6,	9,	3,	2,	15 },
							{ 13,	8,	10,	1,	3,	15,	4,	2,	11,	6,	7,	12,	0,	5,	14,	9 } };

const int DES::S3[4][16] = { { 10,	0,	9,	14,	6,	3,	15,	5,	1,	13,	12,	7,	11,	4,	2,	8 },
							{ 13,	7,	0,	9,	3,	4,	6,	10,	2,	8,	5,	14,	12,	11,	15,	1 },
							{ 13,	6,	4,	9,	8,	15,	3,	0,	11,	1,	2,	12,	5,	10,	14,	7 },
							{ 1,	10,	13,	0,	6,	9,	8,	7,	4,	15,	14,	3,	11,	5,	2,	12 } };

const int DES::S4[4][16] = { { 7,	13,	14,	3,	0,	6,	9,	10,	1,	2,	8,	5,	11,	12,	4,	15 },
							{ 13,	8,	11,	5,	6,	15,	0,	3,	4,	7,	2,	12,	1,	10,	14,	9 },
							{ 10,	6,	9,	0,	12,	11,	7,	13,	15,	1,	3,	14,	5,	2,	8,	4 },
							{ 3,	15,	0,	6,	10,	1,	13,	8,	9,	4,	5,	11,	12,	7,	2,	14 } };

const int DES::S5[4][16] = { { 2,	12,	4,	1,	7,	10,	11,	6,	8,	5,	3,	15,	13,	0,	14,	9 },
							{ 14,	11,	2,	12,	4,	7,	13,	1,	5,	0,	15,	10,	3,	9,	8,	6 },
							{ 4,	2,	1,	11,	10,	13,	7,	8,	15,	9,	12,	5,	6,	3,	0,	14 },
							{ 11,	8,	12,	7,	1,	14,	2,	13,	6,	15,	0,	9,	10,	4,	5,	3 } };

const int DES::S6[4][16] = { { 12,	1,	10,	15,	9,	2,	6,	8,	0,	13,	3,	4,	14,	7,	5,	11 },
							{ 10,	15,	4,	2,	7,	12,	9,	5,	6,	1,	13,	14,	0,	11,	3,	8 },
							{ 9,	14,	15,	5,	2,	8,	12,	3,	7,	0,	4,	10,	1,	13,	11,	6 },
							{ 4,	3,	2,	12,	9,	5,	15,	10,	11,	14,	1,	7,	6,	0,	8,	13 } };

const int DES::S7[4][16] = { { 4,	11,	2,	14,	15,	0,	8,	13,	3,	12,	9,	7,	5,	10,	6,	1 },
							{ 13,	0,	11,	7,	4,	9,	1,	10,	14,	3,	5,	12,	2,	15,	8,	6 },
							{ 1,	4,	11,	13,	12,	3,	7,	14,	10,	15,	6,	8,	0,	5,	9,	2 },
							{ 6,	11,	13,	8,	1,	4,	10,	7,	9,	5,	0,	15,	14,	2,	3,	12 } };

const int DES::S8[4][16] = { { 13,	2,	8,	4,	6,	15,	11,	1,	10,	9,	3,	14,	5,	0,	12,	7 },
							{ 1,	15,	13,	8,	10,	3,	7,	4,	12,	5,	6,	11,	0,	14,	9,	2 },
							{ 7,	11,	4,	1,	9,	12,	14,	2,	0,	6,	10,	13,	15,	3,	5,	8 },
							{ 2,	1,	14,	7,	4,	10,	8,	13,	15,	12,	9,	0,	3,	5,	6,	11 } };


//Konwertery
void DES::IntConvert(int value, bool result[]) {
	int mod;
	for (int i = 3; i >= 0; --i) {
		mod = value % 2;
		value /= 2;
		result[i] = mod;
	}
}

void DES::ByteToBitConverter(uint8_t byte, bool converted[8]) {
	
	for (int i = 0, j = 7; i < 8; ++i)
		converted[j--] = byte & (1 << i);
}

void DES::BitToByteConverter(uint8_t bytes[], bool output[64]) {
	int value = 0;
	for (int i = 0, j = 0; i < 64; i += 8)
	{
		value = output[i] * 128 + output[i + 1] * 64 + output[i + 2] * 32 + output[i + 3] * 16 + output[i + 4] * 8 + output[i + 5] * 4 + output[i + 6] * 2 + output[i + 7] * 1;
		bytes[j++] = (uint8_t)value;
	}
}

void DES::SboxConvert(bool partSbox[], int boxNr) {
	int column = partSbox[1] * 8 + partSbox[2] * 4 + partSbox[3] * 2 + partSbox[4] * 1;
	int row = partSbox[0] * 2 + partSbox[5] * 1;
	int value;
	switch (boxNr) {
	case 1:
		value = S1[row][column];
		break;
	case 2:
		value = S2[row][column];
		break;
	case 3:
		value = S3[row][column];
		break;
	case 4:
		value = S4[row][column];
		break;
	case 5:
		value = S5[row][column];
		break;
	case 6:
		value = S6[row][column];
		break;
	case 7:
		value = S7[row][column];
		break;
	case 8:
		value = S8[row][column];
		break;
	}
	IntConvert(value, partSbox);
}

//Kroki wykonywane dla klucza
void DES::permutedChoiceOne(bool key[], bool c[], bool d[]) {

	for (int i = 0, k = 0, l = 0; i < 28; i++)
		for (int j = 0; j < 64; j++)
		{
			if (PC1L[i] - 1 == j)
				c[k++] = key[j];

			if (PC1R[i] - 1 == j)
				d[l++] = key[j];
		}
}

void DES::permutedChoiceTwo(std::vector<bool> &result, bool c[], bool d[]) {
	bool tmp[56];
	for (int i = 0, j = 28; i < 28; ++i, ++j)
	{
		tmp[i] = c[i];
		tmp[j] = d[i];
	}
	for (int i = 0; i < 48; i++)
		for (int j = 0; j < 56; j++)
			if (PC2[i] - 1 == j)
				result.push_back(tmp[j]);
}

void DES::leftShift(int keyRound, bool subKey[]) {
	bool tmp[28];
	bool tmp2[28];
	if (keyRound == 1 || keyRound == 2 || keyRound == 9 || keyRound == 16) {
		tmp2[27] = subKey[0];
		for (int i = 0, j = 1; j < 28; ++i, ++j) {
			tmp2[i] = subKey[j];
		}
	}
	else {
		tmp[27] = subKey[0];
		for (int i = 0, j = 1; j < 28; ++i, ++j) {
			tmp[i] = subKey[j];
		}
		tmp2[27] = tmp[0];
		for (int i = 0, j = 1; j < 28; ++i, ++j) {
			tmp2[i] = tmp[j];
		}
	}
	for (int i = 0; i < 28; ++i) 
		subKey[i] = tmp2[i];
}

void DES::rightShift(int keyRound, bool subKey[]) {
	if (keyRound == 1) return;
	bool tmp[28];
	bool tmp2[28];
	if (keyRound == 2 || keyRound == 9 || keyRound == 16) {
		tmp2[0] = subKey[27];
		for (int i = 27, j = 26; i >= 1; --i, --j) 
			tmp2[i] = subKey[j];
	}
	else {
		tmp[0] = subKey[27];
		for (int i = 27, j = 26; i >= 1; --i, --j) 
			tmp[i] = subKey[j];
		
		tmp2[0] = tmp[27];

		for (int i = 27, j = 26; i >= 1; --i, --j) 
			tmp2[i] = tmp[j];
	}
	for (int i = 0; i < 28; ++i) 
		subKey[i] = tmp2[i];
}

//Poczatek szyfrowania DES'a
void DES::initialPermutation(bool plaintext[], bool toPermute[]) {
	for (int i = 0; i < 64; ++i)
		for (int j = 0; j < 64; ++j)
			if (IP[i] - 1 == j) {
				toPermute[i] = plaintext[j];
				break;
			}
}
void DES::splitText(bool permutated[], bool Left[], bool Right[]) {
	for (int i = 0, j = 32; j < 64; ++i, ++j) {
		Left[i] = permutated[i];
		Right[i] = permutated[j];
	}
}

//Kroki wykonywane co przebieg
void DES::copyToLeft(bool Right[], bool permutated[]) {
	for (int i = 0; i < 32; ++i)
		permutated[i] = Right[i];
}

void DES::f(bool Left[], bool Right[], std::vector<bool> key) {
	bool Expansion[48];
	bool xored[48];
	for (int i = 0; i < 48; ++i) 
		for (int j = 0; j < 32; ++j) 
			if (E[i] - 1 == j) {
				Expansion[i] = Right[j];
				break;
			}
	for (int i = 0; i < 48; i++)
		xored[i] = Expansion[i] ^ key.at(i);
	bool *partSbox = new bool[6];
	bool Sbox[32];
	for (int j = 0, k = 0, l = 0; j < 8; ++j) {
		for (int i = 0; i < 6; ++i) 
			partSbox[i] = xored[k++];
		SboxConvert(partSbox, j + 1);
		for (int i = 0; i < 4; ++i) 
			Sbox[l++] = partSbox[i];
	}

	bool permutation[32];
	for (int i = 0; i < 32; ++i)
		for (int j = 0; j < 32; ++j)
			if (P[i] - 1 == j) {
				permutation[i] = Sbox[j];
				break;
			}

	for (int i = 0; i < 32; i++)
		Left[i] = Left[i] ^ permutation[i];
	delete[] partSbox;
}

void DES::copyToRight(bool Left[], bool permutated[]) {
	for (int i = 32, j = 0; i < 64; ++i, ++j)
		permutated[i] = Left[j];
}

//Koniec szyfrowania DES'a
void DES::finalPermutation(bool permutated[], bool toplaintext[]) {
	for (int i = 0; i < 64; ++i)
		for (int j = 0; j < 64; ++j)
			if (FP[i] - 1 == j) {
				toplaintext[i] = permutated[j];
				break;
			}
}


template<class Container>
Container binary_load(std::string const & bin_file_name)
{
	std::ifstream in(bin_file_name, std::ios::binary);
	if (!in) {
		throw std::runtime_error("Could not open \"" + bin_file_name + "\" for reading");
	}
	std::noskipws(in);
	return Container(std::istream_iterator<std::uint8_t>(in), std::istream_iterator<std::uint8_t>());

}

template<class Container>
void binary_save(Container && data, std::string const & bin_file_name)
{
	std::ofstream out(bin_file_name, std::ios::binary);
	if (!out) {
		throw std::runtime_error("Could not open \"" + bin_file_name + "\" for writing");
	}
	std::copy(data.begin(), data.end(), std::ostream_iterator<std::uint8_t>(out, ""));
}

DES::DES(){

}

DES::DES(const std::string keyFile, std::string file)
{
	try {
		std::vector<uint8_t> keyByte(binary_load< std::vector<uint8_t> >(keyFile));
		wholeFile = binary_load< std::vector<uint8_t> >(file);

		[key = key](auto keyByte)  {
			bool keyChar[8] = {0, 0, 0, 0, 0, 0, 0, 0};
			int indexK = 0;
			for each (auto var in keyByte)
			{
				ByteToBitConverter(var, keyChar);
				for (int j = 0; j < 8; ++j) {
					key[indexK++] = keyChar[j];
				}
				if (indexK >= 64) break;
			}
		}(keyByte);
	}
	catch (std::runtime_error e) {
		std::cout << e.what() << std::endl;
	}
}

DES::~DES()
{
	delete L, R, c, d;
	keySchedule.clear();
	for (int i = 0; i < 64; ++i) {
		plainText[i] = 0;
		key[i] = 0;
	}
}

void DES::cipher() {
	keySchedule.clear();
	std::vector<bool> partKey;
	permutedChoiceOne(key, c, d);
	int keyRound = 0;
	while (keyRound < 16) {
		leftShift(keyRound + 1, c);
		leftShift(keyRound + 1, d);
		keySchedule.push_back(partKey);
		permutedChoiceTwo(keySchedule.at(keyRound++), c, d);
	}
	processData(1);
}

void DES::decipher() {
	keySchedule.clear();
	std::vector<bool> partKey;
	permutedChoiceOne(key, c, d);
	int keyRound = 0;
	while (keyRound < 16) {
		rightShift(keyRound + 1, c);
		rightShift(keyRound + 1, d);
		keySchedule.push_back(partKey);
		permutedChoiceTwo(keySchedule.at(keyRound++), c, d);
	}
	processData(0);
	
}

void DES::processData(bool choice) {
	processedFile.clear();
	uint8_t pData[8];
	bool encryption[64];
	bool encrypted[64];
	bool keyChar[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	int remain = 0;
	unsigned int indexJ = 0;
	bool remaining[8];
	for (int i = 0; i < 8; ++i) {
		remaining[i] = 0;
	}
	for (int i = 0; i < ceil((float)wholeFile.size() / 8.0) + 1; ++i) {
		int indexK = 0;
		if (i + 1 == ceil((float)wholeFile.size() / 8.0) + 1 && choice == 1) {
			while (indexK <= 55)
				plainText[indexK++] = false;
			for (int r = 0; r < 8; ++r) {
				plainText[indexK++] = remaining[r];
			}
		}
		else {
			for (int a = 0; a < 8; ++a)
			{
				if (indexJ >= wholeFile.size()) {
					while (indexK < 64 && choice == 1) {
						remain++;
						plainText[indexK++] = false;
						if (indexK >= 56) {
							remain += 8;
							ByteToBitConverter(remain / 8, remaining);
							for (int r = 0; r < 8; ++r) {
								plainText[indexK++] = remaining[r];
							}
						}
					}
					break;
				}
				ByteToBitConverter(wholeFile.at(indexJ++), keyChar);
				for (int j = 0; j < 8; ++j) {
					plainText[indexK++] = keyChar[j];
				}
				if (indexK >= 64) break;
			}
		}
		initialPermutation(plainText, encryption);
		splitText(encryption, L, R);

		int round = 0;
		while (round++ < 16) {
			f(L, R, keySchedule.at(round - 1));
			copyToLeft(R, encryption);
			copyToRight(L, encryption);
			splitText(encryption, L, R);
		}

		copyToLeft(R, encryption);
		copyToRight(L, encryption);
		finalPermutation(encryption, encrypted);
		BitToByteConverter(pData, encrypted);
		for (int i = 0; i < 8; ++i)
			processedFile.push_back(pData[i]);

		if (i + 1 == ceil((float)wholeFile.size() / 8.0) && choice != 1) {
			BitToByteConverter(pData, encrypted);
			int a = pData[7] - '\0';
			if (processedFile.size() > 9 && processedFile.at(processedFile.size() - 9) == pData[7]) {
				for (int x = a + 7; x >= 0; --x)
					processedFile.pop_back();
				break;
			}
			else if (a == 0) {
				for (int x = 7; x >= 0; --x)
					processedFile.pop_back();
				break;
			}
		}
	}
}

void DES::saveToFile(std::string file) {
	try {
		binary_save(processedFile, file);
	}
	catch (std::runtime_error e) {
		std::cout << e.what() << std::endl;
	}
}


void DES3::keyValue(bool k[64], std::vector<uint8_t> keyByte) {
	bool keyChar[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	int indexK = 0;
	for each (auto var in keyByte)
	{
		ByteToBitConverter(var, keyChar);
		for (int j = 0; j < 8; ++j) 
			k[indexK++] = keyChar[j];
		if (indexK >= 64) break;
	}
}

void DES3::cipherChosenKey(bool key[64]) {
	keySchedule.clear();
	std::vector<bool> partKey;
	permutedChoiceOne(key, c, d);
	int keyRound = 0;
	while (keyRound < 16) {
		leftShift(keyRound + 1, c);
		leftShift(keyRound + 1, d);
		keySchedule.push_back(partKey);
		permutedChoiceTwo(keySchedule.at(keyRound++), c, d);
	}
	processData(1);
}

void DES3::decipherChosenKey(bool key[64]) {
	keySchedule.clear();
	std::vector<bool> partKey;
	permutedChoiceOne(key, c, d);
	int keyRound = 0;
	while (keyRound < 16) {
		rightShift(keyRound + 1, c);
		rightShift(keyRound + 1, d);
		keySchedule.push_back(partKey);
		permutedChoiceTwo(keySchedule.at(keyRound++), c, d);
	}
	processData(0);
}

DES3::DES3(std::string keyFile1, std::string keyFile2, std::string keyFile3, std::string file) 
{
	try {
		std::vector<uint8_t> keyByte1(binary_load< std::vector<uint8_t> >(keyFile1));
		std::vector<uint8_t> keyByte2(binary_load< std::vector<uint8_t> >(keyFile2));
		std::vector<uint8_t> keyByte3(binary_load< std::vector<uint8_t> >(keyFile3));
		wholeFile = binary_load< std::vector<uint8_t> >(file);
		keyValue(key1, keyByte1);
		keyValue(key2, keyByte2);
		keyValue(key3, keyByte3);
	}
	catch (std::runtime_error e) {
		std::cout << e.what() << std::endl;
	}
}
DES3::~DES3() {
	keySchedule.clear();
	for (int i = 0; i < 64; ++i) {
		plainText[i] = 0;
		key1[i] = 0;
		key2[i] = 0;
		key3[i] = 0;
	}
}

void DES3::cipher() {
	cipherChosenKey(key1);
	cipherChosenKey(key2);
	cipherChosenKey(key3);
}

void DES3::decipher() {
	decipherChosenKey(key1);
	decipherChosenKey(key2);
	decipherChosenKey(key3);
}