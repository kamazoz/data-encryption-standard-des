// DES.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "DES.h"

void KSToString(std::vector<bool>);

int main()
{
	std::string mExit = "9";
	while (mExit != "t") {
		std::string desChoice = "a";
		std::cout << "Wybierz:\n1.DES1\n2.DES3" << std::endl;
		std::cin >> desChoice;
		if (desChoice == "1") {
			std::string key;
			std::string file;
			std::string choice;
			std::ifstream checkFiles;
			int keyGiven = 0;
			std::cout << "Podaj plik z kluczem." << std::endl;
			std::cin >> key;
			std::cout << "Podaj plik do przetworzenia." << std::endl;
			std::cin >> file;
			DES des(key, file);
			choice = "t";
			while (choice != "1" && choice != "2") {
				std::cout << "1.Szyfrowanie\n2.Deszyfrowanie" << std::endl;
				std::cin >> choice;
				if (choice == "1")
					des.cipher();
				else if (choice == "2")
					des.decipher();
			}
			std::cout << "Podaj nazwe pliku do ktorego chcesz zapisac dane." << std::endl;
			std::cin >> file;
			des.saveToFile(file);
		}
		else if (desChoice == "2") {
			std::string key1;
			std::string key2;
			std::string key3;
			std::string file;
			std::string choice;
			std::ifstream checkFiles;
			int keyGiven = 0;
			std::cout << "Podaj 1 plik z kluczem." << std::endl;
			std::cin >> key1;
			std::cout << "Podaj 2 plik z kluczem." << std::endl;
			std::cin >> key2;
			std::cout << "Podaj 3 plik z kluczem." << std::endl;
			std::cin >> key3;
			std::cout << "Podaj plik do przetworzenia." << std::endl;
			std::cin >> file;
			DES3 des(key1, key2, key3, file);
			choice = "t";
			while (choice != "1" && choice != "2") {
				std::cout << "1.Szyfrowanie\n2.Deszyfrowanie" << std::endl;
				std::cin >> choice;
				if (choice == "1")
					des.cipher();
				else if (choice == "2")
					des.decipher();
			}
			std::cout << "Podaj nazwe pliku do ktorego chcesz zapisac dane." << std::endl;
			std::cin >> file;
			des.saveToFile(file);
		}

		std::cout << "Zakonczyc program?\n't' - tak\n'n' - nie" << std::endl;
		std::cin >> mExit;
	}
	return 0;
}

