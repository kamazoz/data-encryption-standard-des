#pragma once
class DES
{
private:
	bool key[64] = { 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0 };
protected:
	static const int IP[64],  FP[64], E[48], P[32], PC1L[28], PC1R[28], PC2[48], 
		S1[4][16], S2[4][16], S3[4][16], S4[4][16], 
		S5[4][16], S6[4][16], S7[4][16], S8[4][16];

	bool plainText[64] = { 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0 };


	std::vector< std::vector<bool> > keySchedule;
	std::vector<uint8_t> wholeFile;
	std::vector<uint8_t> processedFile;
	bool *L = new bool[32];
	bool *R = new bool[32];
	bool *c = new bool[28];
	bool *d = new bool[28];


protected:
	//Konwertery
	static void IntConvert(int value, bool result[]);
	static void ByteToBitConverter(uint8_t byte, bool converted[8]);
	static void BitToByteConverter(uint8_t bytes[], bool output[64]);
	static void SboxConvert(bool partSbox[], int boxNr);

	//Kroki dla klucza
	static void DES::permutedChoiceOne(bool key[], bool c[], bool d[]);
	static void DES::permutedChoiceTwo(std::vector<bool> &result, bool c[], bool d[]);
	static void DES::leftShift(int keyRound, bool subKey[]);
	static void DES::rightShift(int keyRound, bool subKey[]);
	
	//Szyfrowanie DES'a
	static void initialPermutation(bool plaintext[], bool toPermute[]);
	static void splitText(bool permutated[], bool Left[], bool Right[]);

	//Kroki wykonywane co przebieg
	static void copyToLeft(bool Right[], bool permutated[]);
	static void f(bool Left[], bool Right[], std::vector<bool> key);
	static void copyToRight(bool Left[], bool permutated[]);

	static void finalPermutation(bool permutated[], bool toplaintext[]);

public:
	DES();
	DES(std::string keyFile, std::string file);
	virtual ~DES();

	//Szyfrowanie
	virtual void cipher();
	//Deszyfrowanie
	virtual void decipher();
	//Przetwarzanie danych
	void processData(bool choice);
	//Zapis do wybranego pliku
	void saveToFile(std::string file);
};

class DES3 : public DES {
private:
	bool key1[64] = { 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0 };
	bool key2[64] = { 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0 };
	bool key3[64] = { 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0 };
private:
	void keyValue(bool k[64], std::vector<uint8_t> keyByte);
	void cipherChosenKey(bool key[64]);
	void decipherChosenKey(bool key[64]);

public:
	DES3(std::string keyFile1, std::string keyFile2, std::string keyFile3, std::string file);
	~DES3();

	void cipher() override;
	void decipher() override;
};
